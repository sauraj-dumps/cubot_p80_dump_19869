#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_P80.mk

COMMON_LUNCH_CHOICES := \
    lineage_P80-user \
    lineage_P80-userdebug \
    lineage_P80-eng
