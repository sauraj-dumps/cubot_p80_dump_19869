#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from P80 device
$(call inherit-product, device/cubot/P80/device.mk)

PRODUCT_DEVICE := P80
PRODUCT_NAME := lineage_P80
PRODUCT_BRAND := CUBOT
PRODUCT_MODEL := P80
PRODUCT_MANUFACTURER := cubot

PRODUCT_GMS_CLIENTID_BASE := android-cubot

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_mssi_t_64_cn-user 13 TP1A.220624.014 2023362 release-keys"

BUILD_FINGERPRINT := CUBOT/P80_EEA/P80:13/TP1A.220624.014/2023362:user/release-keys
